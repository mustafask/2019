---
duration: 25
presentation_url: null
room: Madison
slot: 2019-10-04 15:45:00-04:00
speakers:
- "Fl\xE1vio Juvenal"
title: Django + ElasticSearch without invalidation logic
type: talk
video_url: null
---

This talk will teach you to finally integrate Django and Elasticsearch "like
it's 2019".

Elasticsearch is a great addition to the Django developer's toolkit: it
supports performant complex full-text queries and filters on huge datasets,
where traditional relational database-only solutions fall short. But
integrating Django with Elasticsearch usually is a pain: you need logic to
keep database tables and Elasticsearch indexes in sync. Since data is stored
in two places, it can become out-of-sync if care is not taken. Dirty index
data will generate wrong search results, defeating the purpose of the
integration.

A new alternative is [django-
zombodb](https://github.com/vintasoftware/django-zombodb), a Django app that
uses a Postgres extension for syncing tables with Elasticsearch indexes at
transaction time. With django-zombodb, developers can treat an ElasticSearch
index just like an internal Postgres index. This means no code is needed to
synchronize Postgres with Elasticsearch, you just need to run a Django
migration that executes a CREATE INDEX in the database and you're done. Any
new inserts, updates or deletes on that model will reflect on an
Elasticsearch index at transaction time!

django-zombodb also offers a Pythonic/Djangonic API to make Elasticsearch
queries over Django models using the ORM in a queryset-friendly way.
Developers are able to compose Elasticsearch queries with regular ORM
queries by just chaining queryset methods and composing Q-like objects. In
this talk, you'll learn django-zombodb advantages over other solutions, how
it works, how to use it, and even you can contribute to it.
