---
duration: 25
presentation_url: null
room: PennTop North
slot: 2019-10-05 16:15:00-04:00
speakers:
- Sarah Schattschneider
title: 'Airflow in Practice: How I Learned to Stop Worrying and Love DAGs'
type: talk
video_url: null
---

Heard of Apache Airflow? Do you work with Apache Airflow or want to work
with Apache Airflow? Ever wonder how to test Airflow better? Have you
considered all data workflow use cases for Airflow? Come be reminded of key
concepts and then we will dive into Airflow’s value add, common use cases,
and best practices. Some of the use cases that will be discussed include,
Extract Transform Load (ETL) jobs, efficiently snapshot databases, and ML
feature extraction.
