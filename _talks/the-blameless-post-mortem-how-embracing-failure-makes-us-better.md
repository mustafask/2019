---
duration: 25
presentation_url: null
room: PennTop South
slot: 2019-10-05 14:30:00-04:00
speakers:
- Christopher Wilcox
title: 'The blameless post mortem: how embracing failure makes us better'
type: talk
video_url: null
---

While developing software, bugs and mistakes are inevitable. Come to hear
how we can improve the approaches we often take as software developers to
work better with one another in heated moments of failure and the aftermath
of incidents. Through better interactions we can build better teams and
create better services.

In my career I have worked in a blameless post-mortem and a blame-full post
mortem environment, across a variety of projects ranging from individual
python libraries, to core infrastructure for a cloud. I am excited to share
how I think not assigning blame when things go wrong results in a better
team and a better product.
