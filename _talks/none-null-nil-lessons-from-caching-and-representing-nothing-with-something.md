---
duration: 25
presentation_url: null
room: Madison
slot: 2019-10-04 16:55:00-04:00
speakers:
- Felice Ho
title: 'None, null, nil: lessons from caching and representing nothing with something'
type: talk
video_url: null
---

Phil Karlton once said, `There are only two hard things in Computer Science:
cache invalidation and naming things`.

This talk is about the value of `nothing`. I will discuss the success of
building a cache for app performance, but how at one point `nothing` took
down production. You will learn factors to consider when caching for APIs
including cache storage, api design, cache invalidation, and serialization
(of null values).
