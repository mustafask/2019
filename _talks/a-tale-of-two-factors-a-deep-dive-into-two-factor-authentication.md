---
duration: 25
presentation_url: null
room: PennTop North
slot: 2019-10-05 14:00:00-04:00
speakers:
- Kelley Robinson
title: 'A Tale of Two Factors: A Deep Dive into Two Factor Authentication'
type: talk
video_url: null
---

Join us as we explore the modern landscape of Two Factor Authentication
(2FA). This talk will discuss the history of authentication, why 2FA is a
necessary security layer, and introduce the different types of factors
available.

We'll dive into a detailed comparison of methods like SMS, Soft Tokens, Push
Authentication, and YubiKeys. From cryptographic security strength to user
experience, we will break down the benefits and downsides for the different
methods and provide guidance for choosing the right methods for your
business. Finally, the session will walk through example Python code for
implementing time-based one time passcodes (Soft Tokens) like you see in
apps like Google Authenticator, Authy, or Duo.
