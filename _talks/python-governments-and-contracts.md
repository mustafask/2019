---
duration: 25
presentation_url: null
room: Madison
slot: 2019-10-05 14:30:00-04:00
speakers:
- Froilan Irizarry
- "Jos\xE9 Padilla"
title: Python, Governments, and Contracts
type: talk
video_url: null
---

After Hurricane Maria struck Puerto Rico in September 2017, the Puerto Rico
Electric Power Authority awarded Whitefish Energy, a small Montana-based
firm with only two employees at the time, a no-bid contract for up to $300
million to repair part of the island's electrical grid. This contract even
had a clause that prevented the government from auditing or reviewing its
labor cost and profit elements. National outrage led to the cancellation of
this suspicious contract, a $200 million contract was awarded to Cobra
Acquisitions, and some months later another $900 million contract was
awarded to the same company, a subsidiary of an Oklahoma-based fossil fuel
company. Why did PREPA enter into any agreements at all with private
contractors when the standard procedure for near-term disaster response is
for utilities to enter into mutual aid agreements with their counterparts in
other states?

In the past few month's I've noticed an increasing amount of investigative
journalism tackling questionable contracts awarded by different government
entities and officials in Puerto Rico. Lack of transparency, costly
mismanagement, and abuse of power can be found throughout. I feel like it's
our duty to call these actions out and question and understand our
government's fiscal responsibility.

The Office of the Comptroller of Puerto Rico publishes awarded contracts
online and they've built a tool that lets you search and even download
contract documents (after they've been redacted of sensitive information).
We decided to use Python and Django to build a tool that would leverage this
data, showing what the government spends every year and how it spends the
money.
