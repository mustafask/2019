---
duration: 25
presentation_url: null
room: PennTop North
slot: 2019-10-05 15:15:00-04:00
speakers:
- Darshan Markandaiah
title: The magic of Python
type: talk
video_url: null
---

Come to this talk to learn more about magic methods in Python. You might already be aware of some magic methods such as `__repr__` and `__len__`, but Python has a number of other methods that aid in writing cleaner, idiomatic code. In this talk, I will enumerate over a range of these magic methods along with example code snippets. I'll then be touching upon Abstract Base Classes that offer free functionality if you implement certain methods on your class. I'll end by introducing Context Managers and describing how you can write your own Context Managers using a few magic methods.
