---
duration: 25
presentation_url: null
room: Madison
slot: 2019-10-05 16:15:00-04:00
speakers:
- Paul Ganssle
title: Build your Python Extensions with Rust!
type: talk
video_url: null
---

When your Python needs to be fast, one powerful tool is the ability to write
compiled extension modules. Normally this is done using the C API, but that
requires managing reference counts and memory allocation and is nearly
impossible to do *correctly* for non-trivial programs. In this talk, we'll
look at how you can write extension modules in Rust - a memory-safe systems
programming language that is increasingly popular as a replacement for C and
C++, and the [most loved language in the Stack Overflow developer survey 4
years running](https://insights.stackoverflow.com/survey/2019#technology-_-
most-loved-dreaded-and-wanted-languages).

This talk will focus on `pyo3` and `milksnake`, two popular libraries that
provide very different approaches for writing Rust extensions; it will cover
a basic introduction to the libraries, and compare their relative strengths
and weaknesses. These will also be compared to C and Cython extension
modules from a performance, safety and maintainability perspective.
