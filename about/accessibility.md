---
title: Accessibility
---

{{ site.data.event.name }} welcomes attendees with disabilities and strives to
be inclusive and accessible.

## Getting there

See <a href="{% link venue/index.md %}">Venue</a> for general information about
the Hotel Pennsylvania.

## Wheelchair accessibility of venue

The main talk tracks for {{ site.data.event.name }} will be using rooms in a
single floor within the Hotel Pennsylvania, accessible by elevator. Additional
rooms, including the speaker ready room, are located on another floor, and may
be accessed by elevator. All {{ site.data.event.name }} rooms are step
free/wheelchair accessible.

## Hearing accessibility

A PA system will be used when attendees are gathered together.

Some social events will involve unamplified small and larger group discussions
which may be difficult to hear if you cannot hear conversation in typical
meeting environments.

PyGotham is pleased to partner with [White Coat
Captioning](https://www.whitecoatcaptioning.com/) to provide CART (Communication
Access Realtime Transcription), otherwise known as live captioning, for all
talks on all tracks in 2019.

Additionally, PyGotham is pleased to partner with [LC Interpreting
Services](https://www.signlanguagenyc.com/) to provide live American Sign
Language interpreters for all talks on all tracks in 2019.

## Vision accessibility

In most sessions within {{ site.data.event.name }}, material will be presented
via projection.

## Questions

If you have accessibility questions or require additional accommodations, please
email us at [organizers@pygotham.org](mailto:organizers@pygotham.org).

(Accessibility info template reused [from AdaCamp
toolkit](https://adacamp.org/adacamp-toolkit/website-content/#accessibility), CC
BY-SA.)
