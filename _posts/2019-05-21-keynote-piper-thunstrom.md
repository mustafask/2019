---
title: "Keynote: Piper Thunstrom"
date: 2019-05-21 00:00:00 -0400
image: /uploads/posts/piper-thunstrom.jpg
excerpt_separator: <!--more-->
---
Piper Thunstrom is a web developer currently working as a Software Engineer at GLG. For the past 3
years, she's been working on ppb, an education focused game library. <!--more--> She's been talking
about making video games since 2014 (with 3 talks on the topic at previous PyGothams!). Piper has
spent time as an organizer in the NYC Python community. She's proudly and visibly queer and has
talked about her experience transitioning while being a public figure. Her current focus is building
open curriculum and better documentation for ppb.
