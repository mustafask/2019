---
name: MongoDB
tier: silver
site_url: https://www.mongodb.com
logo: mongodb.jpg
---
MongoDB is the leading modern, general purpose database platform. We maintain the core database as
well as client libraries, analytics applications, and a hosted service called MongoDB Atlas. The
MongoDB database platform has been downloaded over 60 million times and there have been more than 1
million MongoDB University registrations. We're proud to be headquartered in NYC.
