---
name: Beeswax
tier: social event
site_url: https://beeswax.com/careers/#about-us
logo: beeswax.png
twitter: BeeswaxIO
---
Beeswax is a global technology company whose mission is to build great advertising software.
Beeswax’s "Bidder-as-a-Service™" (BaaS) technology is the first of its kind, enabling digital media
buyers to regain control over their programmatic advertising efforts. The Bidder-as-a-Service is an
evolved version of the traditional digital advertising platform, offering today’s programmatic
buyers the transparency, control, and flexibility they need to be successful. This innovative
product and dedicated people are at the core of Beeswax. We are a software-first company, many of
our engineers bring best practices and experiences from companies such as Google, Facebook, and
Amazon. We value diversity and seek team members with a variety of life and career experiences that
bring a focus on impact and the pragmatism to figure out what needs to be done right and what needs
to be done right now. 
