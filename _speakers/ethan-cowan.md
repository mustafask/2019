---
name: Ethan Cowan
talks:
- "Ethics \u0026 Bias Detection in the Real World"
---
B. Sc. in physics and mathematics from Maryland. I have been working with
Python for about 12 years, including API development with Django and
scientific computing with scipy, pandas, etc. I am currently working on my
masters thesis at Harvard, focusing on ethics and fairness in machine
learning.
