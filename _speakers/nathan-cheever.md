---
name: Nathan Cheever
talks:
- "1000x faster data manipulation: vectorizing with Pandas and Numpy"
---
Hi! I’m a data scientist at [AdvancedMD](http://advancedmd.com). I work almost 
exclusively in Python building data pipelines and predictive models. When I’m 
not in PyCharm, JupyterLab, Slack, or iTerm2 you’ll often find me reading, 
enjoying music, spending time with family, excessively celebrating when I score at point at foosball, 
bike riding, and occasionally playing guitar. I also produce a website for those 
who are looking to get into data science in Utah you can see [here](https://utahdatascientist.org/).
Thanks for stopping by!
