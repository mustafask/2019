---
name: Brad Miro
talks:
- "Distributed Machine Learning with Python"
---
Brad is passionate about educating the world about artificial intelligence
both by empowering developers and improving societal understanding. He is
currently a Developer Programs Engineer at Google where he specializes in
machine learning and big data solutions. Outside of work, Brad can be found
singing, climbing, playing board games and locating the best restaurants in
NYC.
